package com.server.uks.Implement;

import com.server.uks.Dto.PasienDto;
import com.server.uks.Enum.Role;
import com.server.uks.Model.Pasien;
import com.server.uks.Model.StatusPeriksa;
import com.server.uks.Repository.DataRepository;
import com.server.uks.Repository.PasienRepository;
import com.server.uks.Service.PasienService;
import com.server.uks.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PasienImpl implements PasienService {
    @Autowired
    PasienRepository pasienRepository;

    @Autowired
    DataRepository dataRepository;

    @Override
    public Pasien addPasien(PasienDto pasien) {
        Pasien pasien1 = new Pasien();
        pasien1.setData(dataRepository.findById(pasien.getData()).orElseThrow(() -> new NotFoundException("Id not found!")));
        pasien1.setKeluhan(pasien.getKeluhan());
        if (pasien.getStatus().equals("guru")) {
            pasien1.setStatus(Role.guru);
        } else if (pasien.getStatus().equals("siswa")) {
            pasien1.setStatus(Role.siswa);
        } else if (pasien.getStatus().equals("karyawan")) {
            pasien1.setStatus(Role.karyawan);
        }
        pasien1.setPenanganan("Belum Ditangani");
        return pasienRepository.save(pasien1);
    }

    @Override
    public Pasien getById(Long id) {
        return pasienRepository.findById(id).orElseThrow(() -> new NotFoundException("Id not found!"));
    }

    @Override
    public Pasien putPasien(PasienDto pasien, Long id) {
        Pasien pasien1 = pasienRepository.findById(id).orElseThrow(() -> new NotFoundException("Id nor found!"));
        pasien1.setKeluhan(pasien.getKeluhan());
        if (pasien.getStatus().equals("guru")) {
            pasien1.setStatus(Role.guru);
        } else if (pasien.getStatus().equals("siswa")) {
            pasien1.setStatus(Role.siswa);
        } else if (pasien.getStatus().equals("karyawan")) {
            pasien1.setStatus(Role.karyawan);
        }
        pasien1.setData(dataRepository.findById(pasien.getData()).orElseThrow(() -> new NotFoundException("Id not found!")));
        return pasienRepository.save(pasien1);
    }

    @Override
    public List<Pasien> allPasien() {
        return pasienRepository.findAll();
    }

    @Override
    public Map<String, Boolean> deletePasien(Long id) {
        try {
            pasienRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("Deleted!", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id not found!");
        }
    }

    @Override
    public List<Pasien> findStatus(String status) {
        return pasienRepository.findByStatus(Role.valueOf(status));
    }

    @Override
    public Pasien putPeriksa(Long id, StatusPeriksa pasien) {
        Pasien pasien1 = pasienRepository.findById(id).orElseThrow(() -> new NotFoundException("Id not found!"));
        pasien1.setPenanganan("Sudah ditangani");
        return pasienRepository.save(pasien1);
    }
}