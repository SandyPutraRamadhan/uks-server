package com.server.uks.Implement;

import com.server.uks.Model.Diagnosa;
import com.server.uks.Model.PenangananPertama;
import com.server.uks.Repository.PenangananRepository;
import com.server.uks.Service.PenangananService;
import com.server.uks.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PenangananImpl implements PenangananService {

    @Autowired
    PenangananRepository penangananRepository;

    @Override
    public PenangananPertama getPenangananPertama(Long id) {
        return penangananRepository.findById(id).orElseThrow(() -> new NotFoundException("Id tidak ditemukan"));
    }

    @Override
    public PenangananPertama addPenangananPertama(PenangananPertama penangananPertama) {
        penangananPertama.setNamaPenanganan(penangananPertama.getNamaPenanganan());
        return penangananRepository.save(penangananPertama);
    }

    @Override
    public PenangananPertama editPenangananPertama(Long id, PenangananPertama penangananPertama) {
        PenangananPertama penangananPertama1 = penangananRepository.findById(id).orElseThrow(() -> new NotFoundException("Not Found"));
        penangananPertama1.setNamaPenanganan(penangananPertama.getNamaPenanganan());
        return penangananRepository.save(penangananPertama1);
    }

    @Override
    public Map<String, Boolean> deletePenangananPertamaById(Long id) {
        try {
            penangananRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }

    @Override
    public List<PenangananPertama> allPenangananPertama() {
        return penangananRepository.findAll();
    }
}
