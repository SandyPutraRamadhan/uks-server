package com.server.uks.Implement;

import com.server.uks.Dto.PenangananPasienDto;
import com.server.uks.Model.StatusPeriksa;
import com.server.uks.Repository.*;
import com.server.uks.Service.StatusPeriksaService;
import com.server.uks.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class StatusPeriksaImpl implements StatusPeriksaService {
    @Autowired
    StatusPeriksaRepository statusPeriksaRepository;

    @Autowired
    PasienRepository pasienRepository;

    @Autowired
    DiagnosaRepository diagnosaRepository;

    @Autowired
    PenangananRepository penangananRepository;

    @Autowired
    TindakanRepository tindakanRepository;

    @Autowired
    ObatRepository obatRepository;

    @Override
    public StatusPeriksa add(PenangananPasienDto penangananPasien) {
        StatusPeriksa statusPeriksa1 = new StatusPeriksa();
        statusPeriksa1.setPenangananId(penangananRepository.findById(penangananPasien.getPenangananId()).orElseThrow(() -> new NotFoundException("Id not found!")));
        statusPeriksa1.setDiagnosaId(diagnosaRepository.findById(penangananPasien.getDiagnosaId()).orElseThrow(() -> new NotFoundException("Id not found!")));
        statusPeriksa1.setTindakanId(tindakanRepository.findById(penangananPasien.getTindakanId()).orElseThrow(() -> new NotFoundException("Id not found!")));
        statusPeriksa1.setObatId(obatRepository.findById(penangananPasien.getObatId()).orElseThrow(() -> new NotFoundException("Id not found!")));
        return statusPeriksaRepository.save(statusPeriksa1);
    }

    @Override
    public StatusPeriksa getById(Long id) {
        return statusPeriksaRepository.findById(id).orElseThrow(() -> new NotFoundException("Id not found!"));
    }

    @Override
    public List<StatusPeriksa> getAll() {
        return statusPeriksaRepository.findAll();
    }

    @Override
    public Map<String, Boolean> deleteStatusById(Long id) {
        try {
            statusPeriksaRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }

//    @Override
//    public List<StatusPeriksa> getByPasien(Long pasienId) {
//        return statusPeriksaRepository.findByPasien(pasienId);
//    }
}
