package com.server.uks.Implement;

import com.server.uks.Enum.Role;
import com.server.uks.Model.Data;
import com.server.uks.Repository.DataRepository;
import com.server.uks.Service.DataService;
import com.server.uks.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DataImpl implements DataService {
    @Autowired
    DataRepository dataRepository;

    @Override
    public Data addData(Data data) {
        if (data.getStatus().name().equals("siswa")) {
            data.setStatus(Role.siswa);
        } else if (data.getStatus().name().equals("guru")) {
            data.setStatus(Role.guru);
        } else if (data.getStatus().name().equals("karyawan")) {
            data.setStatus(Role.karyawan);
        }
        return dataRepository.save(data);
    }

    @Override
    public Data getById(Long id) {
        return dataRepository.findById(id).orElseThrow(() -> new NotFoundException("Id not found!"));
    }

    @Override
    public List<Data> getAllData() {
        return dataRepository.findAll();
    }

    @Override
    public Data putData(Long id, Data data) {
        Data data1 = dataRepository.findById(id).orElseThrow(() -> new NotFoundException("Id not found!"));
        if (data.getStatus().name().equals("guru")) {
            data1.setStatus(Role.guru);
        } else if (data.getStatus().name().equals("siswa")) {
            data1.setStatus(Role.siswa);
        } else if (data.getStatus().name().equals("karyawan")) {
            data1.setStatus(Role.karyawan);
        }
        data1.setAlamat(data.getAlamat());
        data1.setNama(data.getNama());
        data1.setJabatan(data.getJabatan());
        data1.setTempatLahir(data.getTempatLahir());
        data1.setTglLahir(data.getTglLahir());
        return dataRepository.save(data1);
    }

    @Override
    public Map<String, Boolean> delete(Long id) {
        try {
            dataRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("Deleted!", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id not found!");
        }
    }

    @Override
    public List<Data> findStatus(String status) {
        return dataRepository.findByStatus(Role.valueOf(status));
    }
}
