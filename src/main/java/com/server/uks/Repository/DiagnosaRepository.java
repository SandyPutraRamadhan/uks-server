package com.server.uks.Repository;

import com.server.uks.Model.Diagnosa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DiagnosaRepository extends JpaRepository<Diagnosa, Long> {
}
