package com.server.uks.Repository;

import com.server.uks.Enum.Role;
import com.server.uks.Model.Pasien;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PasienRepository extends JpaRepository<Pasien, Long> {
        List<Pasien> findByStatus(Role status);
}
