package com.server.uks.Repository;

import com.server.uks.Enum.Role;
import com.server.uks.Model.Data;
import com.server.uks.Model.StatusPeriksa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

// membuat repository, untuk membuat query sqlyog
@Repository
public interface DataRepository extends JpaRepository<Data, Long> {
//    query get all by status
    List<Data> findByStatus(Role status);

    @Query(value = "SELECT * FROM table_data  WHERE status = :status", nativeQuery = true)
    List<Data> findByPasien(String status);
}
