package com.server.uks.Repository;

import com.server.uks.Model.Tindakan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TindakanRepository extends JpaRepository<Tindakan, Long> {
}
