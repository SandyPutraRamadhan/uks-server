package com.server.uks.Repository;

import com.server.uks.Model.PenangananPertama;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PenangananRepository extends JpaRepository<PenangananPertama, Long> {
}
