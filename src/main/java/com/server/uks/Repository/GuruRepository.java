package com.server.uks.Repository;

import com.server.uks.Model.Guru;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GuruRepository extends JpaRepository<Guru, Long> {
    @Query(value = "SELECT * FROM table_guru  WHERE " +
            "nama_guru LIKE CONCAT('%',:query, '%')", nativeQuery = true)
    Page<Guru> findGuru(Pageable pageable);
}
