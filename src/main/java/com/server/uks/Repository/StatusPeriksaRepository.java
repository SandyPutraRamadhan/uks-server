package com.server.uks.Repository;

import com.server.uks.Model.StatusPeriksa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StatusPeriksaRepository extends JpaRepository<StatusPeriksa, Long> {
//    @Query(value = "SELECT * FROM table_status_periksa  WHERE pasien_id = :pasienId", nativeQuery = true)
//    List<StatusPeriksa> findByPasien(Long pasienId);
}

