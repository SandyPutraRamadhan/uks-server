package com.server.uks.Dto;

public class PenangananPasienDto {

    private Long diagnosaId;

    private Long penangananId;

    private Long tindakanId;

    private Long obatId;

    public PenangananPasienDto() {
    }


    public Long getDiagnosaId() {
        return diagnosaId;
    }

    public void setDiagnosaId(Long diagnosaId) {
        this.diagnosaId = diagnosaId;
    }

    public Long getPenangananId() {
        return penangananId;
    }

    public void setPenangananId(Long penangananId) {
        this.penangananId = penangananId;
    }

    public Long getTindakanId() {
        return tindakanId;
    }

    public void setTindakanId(Long tindakanId) {
        this.tindakanId = tindakanId;
    }

    public Long getObatId() {
        return obatId;
    }

    public void setObatId(Long obatId) {
        this.obatId = obatId;
    }
}
