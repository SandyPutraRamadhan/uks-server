package com.server.uks.Dto;

//membuat dto status periksa
public class StatusPeriksaDto {
    private String penanganan;

    public String getPenanganan() {
        return penanganan;
    }

    public void setPenanganan(String penanganan) {
        this.penanganan = penanganan;
    }
}
