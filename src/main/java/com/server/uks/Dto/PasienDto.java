package com.server.uks.Dto;

// membuat dto pasien
public class PasienDto {
    private Long data;

    private String keluhan;

    private String status;

    public PasienDto() {
    }

    public Long getData() {
        return data;
    }

    public void setData(Long data) {
        this.data = data;
    }

    public String getKeluhan() {
        return keluhan;
    }

    public void setKeluhan(String keluhan) {
        this.keluhan = keluhan;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
