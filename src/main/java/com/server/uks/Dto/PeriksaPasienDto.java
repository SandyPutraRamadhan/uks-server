package com.server.uks.Dto;

import com.server.uks.Enum.PasienEnum;

public class PeriksaPasienDto {

    private String namaPasien;

    private String keterangan;

    private PasienEnum status;

    public String getNamaPasien() {
        return namaPasien;
    }

    public void setNamaPasien(String namaPasien) {
        this.namaPasien = namaPasien;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public PasienEnum getStatus() {
        return status;
    }

    public void setStatus(PasienEnum status) {
        this.status = status;
    }
}
