package com.server.uks.Login.Service;

import com.server.uks.Login.model.Login;
import com.server.uks.Login.model.Register;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

public interface RegisterService {
    Register registrasi(Register sekolah);

    Map<String, Object> login(Login login);

    Register getById(Long id);

    Register update(Long id, Register sekolah);

    Register updateFoto(Long id , Register register , MultipartFile multipartFile);

    Register updatePassword(Long id, Register register);

    Map<String, Boolean> deleteSekolah(Long id);

    Page<Register> getAll(String query, Long page);
}
