package com.server.uks.Login.Service;

import com.server.uks.Login.Repository.RegisterRepository;
import com.server.uks.Login.jwt.JwtProvider;
import com.server.uks.Login.model.Login;
import com.server.uks.Login.model.Register;
import com.server.uks.exception.InternalErrorException;
import com.server.uks.exception.NotFoundException;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

@Service
public class RegisterImpl implements RegisterService{

    @Autowired
    RegisterRepository repository;
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    JwtProvider jwtProvider;
    @Autowired
    UserDetailsService userDetailsService;
    @Autowired
    PasswordEncoder passwordEncoder;

    private String authories(String email, String password) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        } catch (BadCredentialsException e) {
            throw new InternalErrorException("Email or Password not found");
        }
        UserDetails userDetails = userDetailsService.loadUserByUsername(email);
        return jwtProvider.generateToken(userDetails);
    }

    @Override
    public Register registrasi(Register sekolah) {
        String UserPassword = sekolah.getPassword().trim();
        boolean PasswordIsNotValid = !UserPassword.matches("^(?=.*[0-9])(?=.*[a-z])(?=\\S+$).{8,20}");
        if (PasswordIsNotValid) throw new InternalErrorException("Password not valid!");
        boolean isEmail = Pattern.compile("^(.+)@(\\S+)$")
                .matcher(sekolah.getEmail()).matches();
        if (!isEmail)throw new InternalErrorException("Email Not Valid");
        sekolah.setPassword(passwordEncoder.encode(sekolah.getPassword()));
        sekolah.setUsername(sekolah.getUsername());
        return repository.save(sekolah);
    }

    @Override
    public Map<String, Object> login(Login login) {
        String token = authories(login.getEmail(), login.getPassword());
        Register sekolah ;

//        mengecek email
        boolean isEmail = Pattern.compile("^(.+)@(\\S+)$")
                .matcher(login.getEmail()).matches();
        System.out.println("is Email " + isEmail);

//        jika true, akan menjalankan sistem if
        if(isEmail) {
            sekolah = repository.findByEmail(login.getEmail());
        } else { // jika false, else akan dijalankan, dgn login username
            sekolah = repository.findByUsername(login.getEmail());
        }

        Map<String, Object> response = new HashMap<>();
        response.put("token", token);
        response.put("expired", "15 menit");
        response.put("user", sekolah);
        return response;
    }

    @Override
    public Register getById(Long id) {
        return repository.findById(id).orElseThrow(() -> new NotFoundException("Id tidak ditemukan"));
    }

    @Override
    public Register update(Long id, Register sekolah) {
        Register register = repository.findById(id).orElseThrow(() -> new NotFoundException("Not Found"));
        register.setUsername(sekolah.getUsername());
        register.setAlamat(sekolah.getAlamat());
        register.setNoTelp(sekolah.getNoTelp());
        register.setEmail(sekolah.getEmail());
        boolean isEmail = Pattern.compile("^(.+)@(\\S+)$")
                .matcher(sekolah.getEmail()).matches();
        if (!isEmail)throw new InternalErrorException("Email Not Valid");
        return repository.save(register);
    }

    @Override
    public Register updateFoto(Long id, Register register, MultipartFile multipartFile) {
        Register register1 = repository.findById(id).orElseThrow(() -> new NotFoundException("Id Not Found"));
        String url = convertToBase64(multipartFile);
        register1.setFoto(url);
        return repository.save(register1);
    }

    @Override
    public Register updatePassword(Long id, Register register) {
        Register update = repository.findById(id).orElseThrow(() -> new NotFoundException("Id Not Found"));
        String UserPassword = register.getPassword().trim();
        boolean PasswordIsNotValid = !UserPassword.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,20}");
        if (PasswordIsNotValid) throw new InternalErrorException("Password not valid!");
        update.setPassword(passwordEncoder.encode(register.getPassword()));
        return repository.save(update);
    }

    @Override
    public Map<String, Boolean> deleteSekolah(Long id) {
        try {
            repository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }

    @Override
    public Page<Register> getAll(String query, Long page) {
        Pageable pageable = PageRequest.of(Math.toIntExact(page), 5);
        return repository.findAll(query ,pageable);
    }
    private String convertToBase64(MultipartFile file) {
        String url = "";
        try {
            byte[] byteData = Base64.encodeBase64(file.getBytes());
            String res = new String(byteData);
            url = "data:" + file.getContentType() + ";base64," + res;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return url;
        }
    }
}
