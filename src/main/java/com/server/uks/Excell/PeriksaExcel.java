package com.server.uks.Excell;

import com.server.uks.Model.Pasien;
import com.server.uks.Model.StatusPeriksa;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.transaction.annotation.Transactional;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class PeriksaExcel {
    public static String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

    static String[] HEADER_PASIEN = {"no", "nama_pasien", "status_pasien", "jabatan", "keluhan_pasien", "tanggal_periksa"};

    static String SHEET = "Sheet1";

    @Transactional(readOnly = true)
    public static ByteArrayInputStream download(Pasien periksaList) {
        try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {
            Sheet sheet = workbook.createSheet(SHEET);

            Row headerRow = sheet.createRow(0);

            for (int col = 0; col < HEADER_PASIEN.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(HEADER_PASIEN[col]);
            }

            int rowIdx = 1;
            Row row = sheet.createRow(rowIdx);

            row.createCell(0).setCellValue(periksaList.getData().getId());
            row.createCell(1).setCellValue(periksaList.getData().getNama());
            row.createCell(2).setCellValue(periksaList.getStatus().name());
            row.createCell(3).setCellValue(periksaList.getData().getJabatan());
            row.createCell(4).setCellValue(periksaList.getKeluhan());
            row.createCell(5).setCellValue(periksaList.getTglPeriksa());

            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        } catch (IOException e) {
            throw new RuntimeException("fail to import data to Excel file: " + e.getMessage());
        }
    }
}
