package com.server.uks.Excell;

import com.server.uks.Enum.Role;
import com.server.uks.Model.Data;
import com.server.uks.Model.Pasien;
import com.server.uks.Model.StatusPeriksa;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DataExcell {

    public static String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

    static String[] HEADER_DATA = { "No" ,"nama", "jabatan", "status", "tempat_lahir", "tgl_lahir", "alamat"};

    static String SHEET = "Sheet1";

    public static boolean hasExcelFormat(MultipartFile file) {
        if (!TYPE.equals(file.getContentType())) {
            return false;
        }
        return true;
    }
    public static ByteArrayInputStream dataToExcel(List<Data> dataList) {
        try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {
            Sheet sheet = workbook.createSheet(SHEET);

            Row headerRow = sheet.createRow(0);

            for (int col = 0; col < HEADER_DATA.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(HEADER_DATA[col]);
            }

            int rowIdx = 1;
            for (Data datas : dataList) {
                Row row = sheet.createRow(rowIdx++);

                row.createCell(0).setCellValue(datas.getId());
                row.createCell(1).setCellValue(datas.getNama());
                row.createCell(2).setCellValue(datas.getJabatan());
                row.createCell(3).setCellValue(datas.getStatus().name());
                row.createCell(4).setCellValue(datas.getTempatLahir());
                row.createCell(5).setCellValue(datas.getTglLahir());
                row.createCell(6).setCellValue(datas.getAlamat());
            }

            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        } catch (IOException e) {
            throw new RuntimeException("fail to import data to Excel file: " + e.getMessage());
        }
    }

    public static List<Data> exceltoData(InputStream is) {

        try {
            Workbook workbook = new XSSFWorkbook(is);

            Sheet sheet = workbook.getSheet(SHEET);
            Iterator<Row> rows = sheet.iterator();

            List<Data> dataList = new ArrayList<Data>();
            int rowNumber = 0;
            while (rows.hasNext()) {
                Row currentRow = rows.next();

                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }

                Iterator<Cell> cellsInRow = currentRow.iterator();

                Data data = new Data();

                int cellIdx = 0;
                while (cellsInRow.hasNext()) {
                    Cell currentCell = cellsInRow.next();

                    switch (cellIdx) {
                        case 0:
                            data.setId((long) currentCell.getNumericCellValue());
                            break;
                        case 1:
                            data.setNama(currentCell.getStringCellValue());
                            break;
                        case 2:
                            data.setJabatan(currentCell.getStringCellValue());
                            break;
                        case 3:
                            data.setStatus(Role.valueOf(currentCell.getStringCellValue()));
                            break;
                        case 4:
                            data.setTempatLahir(currentCell.getStringCellValue());
                            break;
                        case 5:
                            data.setTglLahir(currentCell.getStringCellValue());
                            break;
                        case 6:
                            data.setAlamat(currentCell.getStringCellValue());
                            break;
                        default:
                            break;

                    }
                    cellIdx++;

                }
                dataList.add(data);
            }
            workbook.close();
            return dataList;
        } catch (IOException e) {
            throw new RuntimeException("fail to parse Excel file: " + e.getMessage());
        }
    }
}
