package com.server.uks.Excell;

import com.server.uks.Enum.Role;
import com.server.uks.Model.Data;
import com.server.uks.Model.Pasien;
import com.server.uks.Model.StatusPeriksa;
import com.server.uks.Repository.DataRepository;
import com.server.uks.Repository.PasienRepository;
import com.server.uks.Repository.StatusPeriksaRepository;
import com.server.uks.Service.PasienService;
import com.server.uks.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

@Service
public class DataExcelService {

    @Autowired
    DataRepository dataRepository;

    @Autowired
    PasienRepository pasienRepository;

    @Autowired
    StatusPeriksaRepository statusPeriksaRepository;

    public ByteArrayInputStream load(String status) {
        List<Data> data = dataRepository.findByStatus(Role.valueOf(status));
        ByteArrayInputStream in = DataExcell.dataToExcel(data);
        return in;
    }

//    @Transactional(readOnly = true)
//    public ByteArrayInputStream download(Long id) {
//        Pasien periksa = pasienRepository.findById(id).orElseThrow(() -> new NotFoundException("Id not found"));
//        StatusPeriksa statusPeriksaList = statusPeriksaRepository.findByPasien(periksa.getId());
//        ByteArrayInputStream in = DataExcell.download(periksa , statusPeriksaList);
//        return in;
//    }

    public void saveData(MultipartFile file) {
        try {
            List<Data> dataList = DataExcell.exceltoData(file.getInputStream());
            dataRepository.saveAll(dataList);
        } catch (IOException e) {
            throw new RuntimeException("fail to store excel data: " + e.getMessage());
        }
    }


}
