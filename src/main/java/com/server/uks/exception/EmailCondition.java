package com.server.uks.exception;

public class EmailCondition extends RuntimeException {
    public EmailCondition(String message) {
        super(message);
    }
}
