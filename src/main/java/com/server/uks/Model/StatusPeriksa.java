package com.server.uks.Model;

import javax.persistence.*;

@Entity
@Table(name = "table_status_periksa")
public class StatusPeriksa {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

//    @ManyToOne
//    @JoinColumn(name = "pasien_id")
//    private Pasien pasienId;

    @ManyToOne
    @JoinColumn(name = "diagnosa_id")
    private Diagnosa diagnosaId;

    @ManyToOne
    @JoinColumn(name = "penanganan_id")
    private PenangananPertama penangananId;

    @ManyToOne
    @JoinColumn(name = "tindakan_id")
    private Tindakan tindakanId;

    @ManyToOne
    @JoinColumn(name = "obat_id")
    private Obat obatId;

    public StatusPeriksa() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Diagnosa getDiagnosaId() {
        return diagnosaId;
    }

    public void setDiagnosaId(Diagnosa diagnosaId) {
        this.diagnosaId = diagnosaId;
    }

    public PenangananPertama getPenangananId() {
        return penangananId;
    }

    public void setPenangananId(PenangananPertama penangananId) {
        this.penangananId = penangananId;
    }

    public Tindakan getTindakanId() {
        return tindakanId;
    }

    public void setTindakanId(Tindakan tindakanId) {
        this.tindakanId = tindakanId;
    }

    public Obat getObatId() {
        return obatId;
    }

    public void setObatId(Obat obatId) {
        this.obatId = obatId;
    }
}
