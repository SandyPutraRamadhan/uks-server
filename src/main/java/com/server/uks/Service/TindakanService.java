package com.server.uks.Service;

import com.server.uks.Model.Diagnosa;
import com.server.uks.Model.Tindakan;

import java.util.List;
import java.util.Map;

public interface TindakanService {

    Tindakan getTindakan(Long id);

    Tindakan addTindakan(Tindakan tindakan);

    Tindakan editTindakan(Long id,Tindakan tindakan);

    Map<String ,Boolean> deleteTindakanById(Long id);

    List<Tindakan> allTindakan();
}
