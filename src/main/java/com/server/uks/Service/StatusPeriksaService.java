package com.server.uks.Service;

import com.server.uks.Dto.PenangananPasienDto;
import com.server.uks.Model.StatusPeriksa;

import java.util.List;
import java.util.Map;

public interface StatusPeriksaService {
    StatusPeriksa add(PenangananPasienDto penangananPasien);

    StatusPeriksa getById(Long id);

    List<StatusPeriksa> getAll();

    Map<String ,Boolean> deleteStatusById(Long id);

//    List<StatusPeriksa> getByPasien(Long pasienId);
}
