package com.server.uks.Service;

import com.server.uks.Model.Diagnosa;
import com.server.uks.Model.PenangananPertama;

import java.util.List;
import java.util.Map;

public interface PenangananService {

    PenangananPertama getPenangananPertama(Long id);

    PenangananPertama addPenangananPertama(PenangananPertama penangananPertama);

    PenangananPertama editPenangananPertama(Long id,PenangananPertama penangananPertama);

    Map<String ,Boolean> deletePenangananPertamaById(Long id);

    List<PenangananPertama> allPenangananPertama();
}
