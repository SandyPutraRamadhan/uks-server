package com.server.uks.Service;

import com.server.uks.Model.Guru;
import com.server.uks.Model.Karyawan;

import java.util.List;
import java.util.Map;

public interface KaryawanService {

    Karyawan getKaryawan(Long id);

    Karyawan addKaryawan(Karyawan karyawan);

    Karyawan editKaryawan(Long id,Karyawan karyawan);

    Map<String ,Boolean> deleteKaryawanById(Long id);

    List<Karyawan> allKaryawan();
}
