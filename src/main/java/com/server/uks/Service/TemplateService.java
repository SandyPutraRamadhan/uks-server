package com.server.uks.Service;

import com.server.uks.Enum.Role;
import com.server.uks.Excell.DataExcell;
import com.server.uks.Excell.Template;
import com.server.uks.Model.Data;
import com.server.uks.Model.Guru;
import com.server.uks.Repository.DataRepository;
import com.server.uks.Repository.GuruRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.util.List;

@Service
public class TemplateService {

    @Autowired
    DataRepository dataRepository;

    public ByteArrayInputStream load(String status) {
        List<Data> data = dataRepository.findByStatus(Role.valueOf(status));
        ByteArrayInputStream in = Template.templateToExcel(data);
        return in;
    }
}
