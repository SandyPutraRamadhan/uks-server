package com.server.uks.Service;

import com.server.uks.Model.Obat;

import java.util.List;
import java.util.Map;

public interface ObatService {

    Obat getObat(Long id);

    Obat addObat(Obat obat);

    Obat editObat(Long id,Obat obat);

    Map<String ,Boolean> deleteObatById(Long id);

    List<Obat> allObat();
}
