package com.server.uks.Service;

import com.server.uks.Model.Guru;

import java.util.List;
import java.util.Map;

public interface GuruService {

    Guru getGuru(Long id);

    Guru addGuru(Guru guru);

    Guru editGuru(Long id,Guru guru);

    Map<String ,Boolean> deleteGuruById(Long id);

    List<Guru> allGuru();
}
