package com.server.uks.Service;

import com.server.uks.Excell.PeriksaExcel;
import com.server.uks.Model.Pasien;
import com.server.uks.Repository.PasienRepository;
import com.server.uks.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;

@Service
public class PasienServiceExcell {

    @Autowired
    PasienRepository pasienRepository;

        public ByteArrayInputStream download(Long id) {
        Pasien periksa = pasienRepository.findById(id).orElseThrow(() -> new NotFoundException("Id not found"));
        ByteArrayInputStream in = PeriksaExcel.download(periksa);
        return in;
    }
}
