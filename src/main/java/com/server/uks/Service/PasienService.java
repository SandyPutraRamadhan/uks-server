package com.server.uks.Service;


import com.server.uks.Dto.PasienDto;
import com.server.uks.Model.Pasien;
import com.server.uks.Model.StatusPeriksa;

import java.util.List;
import java.util.Map;

// untuk membuat method atau fungsi
public interface PasienService {
    //    method post
    Pasien addPasien(PasienDto pasien);

    //    method get by id
    Pasien getById(Long id);

    //    method put
    Pasien putPasien(PasienDto pasien, Long id);

    //    method get all
    List<Pasien> allPasien();

    //    method delete
    Map<String, Boolean> deletePasien(Long id);

    //    method get all by status
    List<Pasien> findStatus(String status);

    //    method put pasien setelah ditangani
    Pasien putPeriksa(Long id, StatusPeriksa pasien);

}
