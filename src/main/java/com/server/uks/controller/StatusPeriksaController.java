package com.server.uks.controller;

import com.server.uks.Dto.PenangananPasienDto;
import com.server.uks.Login.response.CommonResponse;
import com.server.uks.Login.response.ResponseHelper;
import com.server.uks.Model.StatusPeriksa;
import com.server.uks.Service.StatusPeriksaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

// controller untuk menjalankan sistem method
@RestController
// alamat controller
@RequestMapping("/status-periksa")
public class StatusPeriksaController {
    @Autowired
    StatusPeriksaService statusPeriksaService;

    //    method post
    @PostMapping
    public CommonResponse<StatusPeriksa> add(@RequestBody PenangananPasienDto penangananPasien) {
        return ResponseHelper.ok(statusPeriksaService.add(penangananPasien));
    }

    //    method get by id
    @GetMapping("/{id}")
    public CommonResponse<StatusPeriksa> getById(@PathVariable("id") Long id) {
        return ResponseHelper.ok(statusPeriksaService.getById(id));
    }

    //    method get all
    @GetMapping
    public CommonResponse<List<StatusPeriksa>> getAll() {
        return ResponseHelper.ok(statusPeriksaService.getAll());
    }


    @DeleteMapping("/{id}") // untuk menghapus data sesuai id
    public CommonResponse <?> deleteById(@PathVariable("id") Long id) {
        return ResponseHelper.ok(statusPeriksaService.deleteStatusById(id));}

    //    method get all by pasienId
//    @GetMapping("/by-pasien")
//    public CommonResponse<List<StatusPeriksa>> getByPasien(@RequestParam(name = "pasien_id") Long pasienId) {
//        return ResponseHelper.ok(statusPeriksaService.getByPasien(pasienId));
//    }

}
