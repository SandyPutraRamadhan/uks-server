package com.server.uks.controller;

import com.server.uks.Dto.PasienDto;
import com.server.uks.Login.response.CommonResponse;
import com.server.uks.Login.response.ResponseHelper;
import com.server.uks.Model.Pasien;
import com.server.uks.Model.StatusPeriksa;
import com.server.uks.Service.PasienService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

// controller untuk menjalankan sistem method
@RestController
// alamat controller
@RequestMapping("/pasien")
public class PasienController {
    @Autowired
    PasienService pasienService;

    //    method post
    @PostMapping
    public CommonResponse<Pasien> addPasien(@RequestBody PasienDto pasien) {
        return ResponseHelper.ok(pasienService.addPasien(pasien));
    }

    //    method get by id
    @GetMapping("/{id}")
    public CommonResponse<Pasien> getById(@PathVariable("id") Long id) {
        return ResponseHelper.ok(pasienService.getById(id));
    }

    //    method get all
    @GetMapping("/all-pasien")
    public CommonResponse<List<Pasien>> allPasien() {
        return ResponseHelper.ok(pasienService.allPasien());
    }

    //    method put
    @PutMapping("/{id}")
    public CommonResponse<Pasien> putPasien(@RequestBody PasienDto pasien, @PathVariable("id") Long id) {
        return ResponseHelper.ok(pasienService.putPasien(pasien, id));
    }

    //    method put penanganan
    @PutMapping("/periksa/{id}")
    public CommonResponse<Pasien> putPeriksa(@PathVariable("id") Long id, @RequestBody StatusPeriksa pasien) {
        return ResponseHelper.ok(pasienService.putPeriksa(id, pasien));
    }

    //    method delete
    @DeleteMapping("/{id}")
    public CommonResponse<Map<String, Boolean>> deletePasien(@PathVariable("id") Long id) {
        return ResponseHelper.ok(pasienService.deletePasien(id));
    }

    //    method get all by status
    @GetMapping("/all-status")
    public CommonResponse<List<Pasien>> findStatus(@RequestParam(name = "status") String status) {
        return ResponseHelper.ok(pasienService.findStatus(status));
    }
}
